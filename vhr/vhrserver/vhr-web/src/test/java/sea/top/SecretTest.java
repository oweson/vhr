package sea.top;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author chengwanli
 * @date 2020/8/26 15:21
 */
public class SecretTest {
    public static void main(String[] args) {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        System.out.println(passwordEncoder.encode("123456"));
        System.out.println(passwordEncoder.matches("123456","$2a$10$97/s9oYYrlxdfe8cSFmnY.uTgRo0tLdRnB6V1FdnyKNq1519TDOxG"));

    }
}
